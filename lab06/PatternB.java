// Anna Moragne
// lab 06  10/11/18
//Purpose: to practice using nested loops 
// Pattern B
import java.util.Scanner;
public class PatternB{
  public static void main (String [] args){
    Scanner scan = new Scanner (System.in);
    int rowsWanted = 0;
    int currentRow = 1;
    String rowPrint = ("");
    int idk = 0;
    System.out.println("Enter an integer in the range from 1-10 for the number of rows you want in the pyramid.");
    boolean rowInt = scan.hasNextInt(); // testing if value input was an integer
    if (rowInt == true){
      rowsWanted = scan.nextInt(); // declaring rows wanted
    }
    while (rowInt == false){
    scan.next();
    System.out.println("The value you inputted is invalid. Please try again and enter a number in the range 1-10");
    rowInt = scan.hasNextInt();
      if (rowInt == true){
         rowsWanted = scan.nextInt();
      }
    }
    while (rowsWanted < 1 || rowsWanted > 10){
      scan.next();
      System.out.println("The number you entered is outside of the range 1-10. Please try again and enter an integer in the range 1-10");
      rowInt = scan.hasNextInt();
        if (rowInt == true){
          rowsWanted = scan.nextInt();
        }
    }
    if (rowInt == true){
        while (rowsWanted<1 || rowsWanted>10){
          scan.next();
          System.out.println("The number you entered is outside of the range 1-10. Please try again and enter an integer in the range 1-10");
          rowInt = scan.hasNextInt();
          rowsWanted = scan.nextInt();
        }
        for (currentRow=1; rowsWanted>0; rowsWanted--){
          if (rowsWanted ==10){
            System.out.println("1 2 3 4 5 6 7 8 9 10");
          }
          if (rowsWanted ==9){
            System.out.println("1 2 3 4 5 6 7 8 9");
          }
          if (rowsWanted ==8){
            System.out.println("1 2 3 4 5 6 7 8");
          }
          if (rowsWanted ==7){
            System.out.println("1 2 3 4 5 6 7");
          }
          if (rowsWanted ==6){
            System.out.println("1 2 3 4 5 6");
          }
          if (rowsWanted ==5){
            System.out.println("1 2 3 4 5");
          }
          if (rowsWanted ==4){
            System.out.println("1 2 3 4");
          }
          if (rowsWanted ==3){
            System.out.println("1 2 3");
          }
          if (rowsWanted ==2){
            System.out.println("1 2");
          }
          if (rowsWanted ==1){
            System.out.println("1");
          }
        }
    }
  }
}