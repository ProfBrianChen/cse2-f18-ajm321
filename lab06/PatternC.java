// Anna Moragne
// lab 06  10/11/18
//Purpose: to practice using nested loops 
// Pattern C
import java.util.Scanner;
public class PatternC{
  public static void main (String [] args){
    Scanner scan = new Scanner (System.in);
    int rowsWanted = 0;
    int currentRow = 1;
    String rowPrint = ("");
    int idk = 0;
    int space = 1;
    System.out.println("Enter an integer in the range from 1-10 for the number of rows you want in the pyramid.");
    boolean rowInt = scan.hasNextInt(); // testing if value input was an integer
    if (rowInt == true){
      rowsWanted = scan.nextInt(); // declaring rows wanted
    }
    while (rowInt == false){
    scan.next();
    System.out.println("The value you inputted is invalid. Please try again and enter a number in the range 1-10");
    rowInt = scan.hasNextInt();
      if (rowInt == true){
         rowsWanted = scan.nextInt();
      }
    }
    while (rowsWanted < 1 || rowsWanted > 10){
      scan.next();
      System.out.println("The number you entered is outside of the range 1-10. Please try again and enter an integer in the range 1-10");
      rowInt = scan.hasNextInt();
        if (rowInt == true){
          rowsWanted = scan.nextInt();
        }
    }
    if (rowInt == true){
        while (rowsWanted<1 || rowsWanted>10){
          scan.next();
          System.out.println("The number you entered is outside of the range 1-10. Please try again and enter an integer in the range 1-10");
          rowInt = scan.hasNextInt();
          rowsWanted = scan.nextInt();
        }
      for (currentRow=1; currentRow <= rowsWanted; currentRow++){
        for (space=9; space>0; space--){
          System.out.print(" ");
        }
        System.out.printf("%s%s %n" ,currentRow, rowPrint);
        rowPrint = currentRow + rowPrint;
      }
    }
  }
}