// Anna Moragne
// lab 06  10/11/18
//Purpose: to practice using nested loops 
// Pattern D
import java.util.Scanner;
public class PatternD{
  public static void main (String [] args){
    Scanner scan = new Scanner (System.in);
    int rowsWanted = 0;
    int currentRow = 1;
    String rowPrint = ("");
    int idk = 0;
    System.out.println("Enter an integer in the range from 1-10 for the number of rows you want in the pyramid.");
    boolean rowInt = scan.hasNextInt(); // testing if value input was an integer
    if (rowInt == true){
      rowsWanted = scan.nextInt(); // declaring rows wanted
    }
    else {
      scan.next();
      while (rowInt = false);
      System.out.println("The value you inputted is invalid. Please try again and enter a number in the range 1-10");
      rowInt = scan.hasNextInt();
      if (rowInt == true){
         rowsWanted = scan.nextInt();
      }
    }
  while (rowInt == true){
    if (rowsWanted<=10 && rowsWanted>=1){
      for (currentRow=1; rowsWanted>0; rowsWanted--){
          if (rowsWanted ==10){
            System.out.println("10 9 8 7 6 5 4 3 2 1");
          }
          if (rowsWanted ==9){
            System.out.println("9 8 7 6 5 4 3 2 1");
          }
          if (rowsWanted ==8){
            System.out.println("8 7 6 5 4 3 2 1");
          }
          if (rowsWanted ==7){
            System.out.println("7 6 5 4 3 2 1");
          }
          if (rowsWanted ==6){
            System.out.println("6 5 4 3 2 1");
          }
          if (rowsWanted ==5){
            System.out.println("5 4 3 2 1");
          }
          if (rowsWanted ==4){
            System.out.println("4 3 2 1");
          }
          if (rowsWanted ==3){
            System.out.println("3 2 1");
          }
          if (rowsWanted ==2){
            System.out.println("2 1");
          }
          if (rowsWanted ==1){
            System.out.println("1");
          }
      }
      break;
    }
    else if (rowsWanted <= 0) {
      while (rowsWanted <= 0){
      scan.next();
      System.out.println("The value you inputted is invalid. Please try again and enter a number in the range 1-10");
      rowInt = scan.hasNextInt();
      }
    }
    else if (rowsWanted >= 11){
      while (rowsWanted >= 11){
      scan.next();
      System.out.println("The value you inputted is invalid. Please try again and enter a number in the range 1-10");
      rowInt = scan.hasNextInt();
      }
    }
  }
}
}
