// CSE 002    
// Anna Moragnw   ajm321
// Homework 08    11/13/2018
import java.util.Scanner;
import java.util.Random;
public class hw08{ 
  public static void printArray (String [] list, int n){
    int i=0;
    for (i=0; i<n; i++){
      System.out.print(list[i] + " ");
    } // will print out the deck of cards in order
  }
  public static String [] shuffle (String [] list){
    System.out.println("Shuffled");
    Random rand = new Random(); // decalring random gnereator 
    for (int j=0; j<52; j++){
      int randomPosition = rand.nextInt(52); // generate a random card
      String temp = list[j];
      list [j] = list[randomPosition];
      list[randomPosition]=temp; // shuffles the deck of cards 
    }
    return list;
  }
  public static String [] getHand (String [] list, int index, int numCards){
    String [] output = new String [numCards];
      Random rand = new Random(); // declaring random generator 
      for (int k=0; k<numCards; k++){
        int randomPosition = rand.nextInt(52); // randomly selects one of the cards
        output [k] = list [randomPosition]; // creates new array that only holds the cards in the hand 
      }
    return output;
  }
public static void main(String[] args) { 
Scanner scan = new Scanner(System.in); 
 //suits club, heart, spade or diamond 
 String[] suitNames={"C","H","S","D"};    // array of suits
String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; // array of card value
String[] cards = new String[52]; 
String[] hand = new String[5]; 
int numCards = 0; 
int again = 1; 
int index = 51;
for (int i=0; i<52; i++){ 
  cards[i]=rankNames[i%13]+suitNames[i/13]; // makes each card unique
  System.out.print(cards[i]+" "); // prints out list of cards in order
} 
System.out.println();
  //printArray(cards); 
shuffle(cards); // call shuffle method
printArray(cards, 52); // call printArray method
while(again == 1){ 
  System.out.println(); 
  System.out.println("How many cards would you like in a hand?");
   numCards = scan.nextInt();
   hand = getHand(cards,index,numCards); // call getHand method
   printArray(hand, numCards); // call printArray method again
   index = index - numCards; // decreases index as to not select the same hand
   System.out.println("Enter a 1 if you want another hand drawn"); 
   again = scan.nextInt(); 
}  
  }
}
