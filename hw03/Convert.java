// Homework 03 CSE02
// Anna Moragne ajm321
// Due 09-18-2018
// Purpose: By inputting number of acres of land affected by a hurricane and the percipitation, calculate the amount of rain in cubic miles
import java.util.Scanner;
public class Convert {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
    // declaring scanner to accept input
    System.out.print("Enter the affected area in acres in the form xxxxx.xx ");
    // prompts user to manualy input number of affected acres
    double Acres = myScanner.nextDouble();
    // declaring affected acres variable
    System.out.print("Enter the rainfall in the affected area in the form xx ");
    double rainFall = myScanner.nextDouble();
    // decalring rainfall variable 
    double producedGallons=27154.29;
      // the amount of gallons of water for one inch of rain over one acre
    double totalGallons=producedGallons * (Acres * rainFall);
    // compute the total amount of rainfall in gallons 
    double cubicMiles=(1/1101117147428.57) * totalGallons;
    // computes the total rainfall in cubic miles 
    System.out.println ("The total amount of rainfall in cubic miles is " + cubicMiles);
    // Displays the total amount of rainfall in cubic meters
  } //end of main method
} // end of class
    
