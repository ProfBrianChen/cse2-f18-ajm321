// Homework 03 CSE 02
// Anna Moragne ajm321
// Due 09-18-2018
/// Purpose: From a manual input of pyramid dimensions, the program will calculate the volume of the pyramid
import java.util.Scanner;
// importing scanner
public class Pyramid {
  public static void main (String [] args) {
    Scanner myScanner = new Scanner (System.in);
    // accepting input by declaring scanner
    System.out.print ("Enter the length of the side of the square base in the form xx.xx ");
    // prompts user to manually input base length 
    double baseLength=myScanner.nextDouble(); // declaring base length variable
    System.out.print ("Enter the height of the pyramid in the form xx.xx ");
    // prompts user to manually input the height of the pyramid
    double height=myScanner.nextDouble(); // declaring height variable 
    double volume; // decalring volume variable 
    volume=(baseLength * baseLength * height)/3;
    //calculating volume of the pyramid 
    System.out.println ("The volume of the pyramid is " + volume + " cubic units.");
  } // end of main method
} // end of class 
    