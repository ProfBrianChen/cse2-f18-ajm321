/// Anna Moragne ajm321 CSE02
// HW 04 9-25-2018
// Purpose: To create a program that simulates the game of Craps and tells user name of the combination of dice they rolled
// This code will be created using switch statements 
import java.util.Scanner;
public class CrapsSwitch {
  public static void main (String [] args) { 
    Scanner scnrOne = new Scanner (System.in); 
    // declaring scanner to accept input 
    System.out.print("Would you like to manually input the roll of the die? Respond 'yes' or 'no'");
    // asks user whether or not they would like to choose the roll of the die or have it be randomly generated for them 
    String manual = scnrOne.nextLine();
    // declaring variable for result of whether or not dice roll will be random 
    int dieOne = 0; // declaring face value of first die 
    int dieTwo = 0; // declaring face value of second die
    switch (manual) {
      case "yes": 
        manual = "yes";
        System.out.print("What would you like the roll of the first die to be, ranging from 1-6?");
        // asking user to manually input value of first die
        dieOne = scnrOne.nextInt();
        System.out.print("What would you like the roll of the second die to be, ranging from 1-6?");
        // asking user to manually input value of the second die 
        dieTwo = scnrOne.nextInt();
        break;
      case "no": 
        manual = "no";
        dieOne = (int)(Math.random() * 6 + 1);
        // randomly generates value for roll of first die
        dieTwo = (int)(Math.random() * 6 +1);
        // randomly generates value for roll of the second die
        break;
    }
    int sum = dieOne + dieTwo ;
    // declaring sum variable. will help with naming rolls and knowing that it doesnt matter which number is rollled first or second 
    System.out.println ("The two dice you rolled resulted in a " + dieOne + " and a " + dieTwo);
    // declaring the variable of the sum of the roll of the dice
    switch (sum) {
      case 2: 
        System.out.println ("This roll is called 'Snake Eyes'"); // assigns each roll a name 
        break;
      case 3: 
        System.out.println ("This roll is called 'Ace Duce'");
        break;
      case 4: 
        if(dieOne == 2){
          System.out.println ("This roll is called a 'Hard Four'");
        }else{
          System.out.println ("This roll is called an 'Easy Four'");
        }
        break;
      case 5:
        System.out.println ("This roll is called 'Fever Five'");
        break;
      case 6:
        if (dieOne == 3){
        System.out.println ("This roll is called a 'Hard Six'");
        }else{
         System.out.println ("This roll is called an 'Easy Six'");
        }
        break;
      case 7:
        System.out.println ("This roll is called 'Seven Out'");
        break;
      case 8:
        if (dieOne == 4){
        System.out.println ("This roll is called a 'Hard Eight'");
        }else {
         System.out.println ("This roll is called an 'Easy Eight'");
        }
        break;
      case 9:
        System.out.println ("This roll is called a 'Nine'");
        break;
      case 10:
        if (dieOne == 5){
        System.out.println ("This roll is called a 'Hard Ten'");
        }else {
          System.out.println ("This roll is called 'Yo-leven'");
        }
        break;
      case 11:
        System.out.println ("This roll is called 'Yo-leven'");
        break;
      case 12:
        System.out.println ("This roll is called 'Boxcars'");
        break;
      default: 
        System.out.println ("Input is invalid");
        break;
    }
  }
}

                