/// Anna Moragne ajm321 CSE02
// HW 04 9-25-2018
// Purpose: To create a program that simulates the game of Craps and tells user name of the combination of dice they rolled
// This code will be created using if and else statements 
import java.util.Scanner;
public class CrapsIf {
  public static void main (String [] args) { 
    Scanner scnr = new Scanner (System.in); 
    // declaring scanner to accept input 
    System.out.print ("Would you like to manually input the roll of the die? Enter 1 for yes or 2 for no. ");
    // asks user whether or not they would like to choose the roll of the die or have it be randomly generated for them 
    int manual=scnr.nextInt();
    // declaring variable for result of whether or not dice roll will be random 
    int dieOne; // declaring face value of first die 
    int dieTwo; // declaring face value of second die
    if (manual == 1) {
      System.out.print("What would you like the roll of the first die to be, ranging from 1-6?");
      // asking user to manually input value of first die
      dieOne = scnr.nextInt();
      System.out.print("What would you like the roll of the second die to be, ranging from 1-6?");
      // asking user to manually input value of the second die 
      dieTwo = scnr.nextInt();
    }
    else {
      dieOne = (int)(Math.random() * 6 + 1);
      // randomly generates value for roll of first die
      dieTwo = (int)(Math.random() * 6 +1);
      // randomly generates value for roll of the second die
    }
    int sum = dieOne + dieTwo; // sums the value of the roll
    System.out.println ("The roll of the first die is " + dieOne);
    // shows user value of the first die 
    System.out.println ("The roll of the second die is " + dieTwo);
    if ( dieOne == 1 & dieTwo == 1 ) {
      System.out.println ("This roll is called 'Snake Eyes'");
    }
    else if ( sum == 3 ) {
      System.out.println ("This roll is called an 'Ace Duce'");
    }
    else if ( sum == 4 & dieOne == 2 ) {
      System.out.println ("This roll is called a 'Hard Four'");
    }
    else if ( sum == 4 ) {
      System.out.println ("This roll is called an 'Easy Four'");
    }
    else if ( sum == 5 ) {
      System.out.println ("This roll is called a 'Fever Five'");
    }
    else if ( sum == 6 & dieOne == 3 ) {
      System.out.println ("This roll is called a 'Hard Six'");
    }
    else if ( sum == 6 ) {
      System.out.println ("This roll is called an 'Easy Six'");
    }
    else if ( sum == 7 ) {
      System.out.println ("This roll is called a 'Seven Out'");
    }
    else if ( sum == 8 & dieOne == 4 ) {
      System.out.println ("This roll is called a 'Hard Eight'");
    }
    else if ( sum == 8 ) {
      System.out.println ("This roll is called an 'Easy Eight'");
    }
    else if ( sum == 9 ) {
      System.out.println ("This roll is called a 'Nine'");
    }
    else if ( sum == 10 & dieOne == 5 ) {
      System.out.println ("This roll is called a 'Hard Ten'");
    }
    else if ( sum == 10 ) {
      System.out.println ("This roll is called an 'Easy Ten'");
    }
    else if ( sum == 11 ) {
      System.out.println ("This roll is called 'Yo-leven'");
    }
    else if ( sum == 12 ) {
      System.out.println ("This roll is called 'Boxcars'");
    }
  }
}

    
    