// Anna Moragne   ajm321
// CSE 002    homework 09
// 11-27-2018
import java.util.Random;
import java.util.Scanner;
public class RemoveElements{
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
  // given code is all above
  
  public static int [] randomInput (){
    int [] array = new int [10]; // initalizing array with index 10
    int i =0;
    Random rand = new Random (); // decalring new randomizer
    for (i=0; i<10; i++){ // setting integers for each array index randomly
      array [i]=rand.nextInt(10);
    }
    return array; // returns array full of random integer 0-9
  }
  public static int [] delete (int [] list, int pos){
    int [] smallerList = new int [9]; //  initalizing array with index 9 since it will only have one fewer input than the original
    int i =0;
    for (i=0; i<pos; i++){
      smallerList[i]=list[i];
    }
    for (i=pos; i<9; i++){ // start with k+1 because inputs above the one taken out will have index with one fewer
      smallerList[i]=list[i+1];
    }
    return smallerList; // returns array with one removed input from the original
  }
  public static int [] remove (int [] list, int target){
    int count=0;
    for (int i=0; i<10; i++){
      if (list[i]==target){ // finds how many occurances of the target integer there is 
        count++;
      }
    }
    int [] missingList = new int [list.length-count];
    int j=0;
    int index=0;
    for (j=0; j<list.length-count; j++){ 
      if (list[j]!=target){ // if value is not equal to zero 
        missingList[j]=list[index];
        index = index+1;
      }
      else if (list[j]==target){
        index=index+1;
        missingList[j]=list[index];
        index = index+1;
      }
    }
    return missingList;
  }
}