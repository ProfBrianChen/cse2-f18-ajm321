// Anna Moragne   ajm321
// CSE 002  hw 09
// 11-23-2018
import java.util.Scanner;
import java.util.Random;
public class CSE2Linear {
  public static void linearSearch (int [] array, int search){
    int count=0;
    for (int i=0; i<15; i++){
      if (array[i]==search){
        count++;
      }
    }
    if (count>0){
      System.out.println("The grade " + search + " apears " + count + " times.");
    }
    else {
      System.out.println("There were no grades that matched " + search);
    }
  }
  public static void binarySearch (int [] array, int search){
    int count =0;
    if (array [7]>=search){
      for (int a=6; a>=0; a--){
        if(array[a]==search){
          count++;
        }
      }
      System.out.println("The grade " + search + " appeared " + count + " times");
    }
    else if (array[7]<=search){
      for (int b=8; b<15; b++){
        if(array[b]==search){
          count++;
        }
      }
      System.out.println("The grade " + search + " apeared " + count + " times.");
    }
    else {
      System.out.println("There were no occurances of " + search);
    }
  }
  public static int [] scramble (int [] array){
    Random rand = new Random(); // decalring random gnereator 
    for (int j=0; j<15; j++){
      int randomPosition = rand.nextInt(15); // generate a random card
      int temp = array[j];
      array [j] = array[randomPosition];
      array[randomPosition]=temp; // shuffles the deck of cards 
    }
    System.out.println("Scrambled: ");
    for (int k=0; k<15; k++){
      System.out.print(array[k] + " ");
    }
    System.out.println(" ");
    return array;
  }
  public static void main (String [] args){
    Scanner scan = new Scanner(System.in);
    int [] original=new int [15];
    System.out.println("Please enter 15 ascending integers between 0 and 100 for final grades in CSE2");
    original[0]=scan.nextInt();
    original[1]=scan.nextInt();
    original[2]=scan.nextInt();
    original[3]=scan.nextInt();
    original[4]=scan.nextInt();
    original[5]=scan.nextInt();
    original[6]=scan.nextInt();
    original[7]=scan.nextInt();
    original[8]=scan.nextInt();
    original[9]=scan.nextInt();
    original[10]=scan.nextInt();
    original[11]=scan.nextInt();
    original[12]=scan.nextInt();
    original[13]=scan.nextInt();
    original[14]=scan.nextInt();
    boolean intTruth;
    int error=0;
    for (int j=0; j<15; j++){
      intTruth = scan.hasNextInt();
      if (intTruth==false){ // tests if all inputs are integers
        System.out.print("Error: One of the values you input is not an integer.");
        error++;
        break;
      }
      else if (original[j]<0){
        System.out.println("Error: One of your inputs was outside of the range 0-100.");
        error++;
        break;
      }
      else if (original[j]>100){
        System.out.println("Error: One of your inputs was outside of the range 0-100.");
        error++;
        break;
      }
    } // end of first set of error tests
    for (int k=0; k<14; k++){
      if (original[k]>original[k+1]){ // tests if input values are in ascending order
        System.out.println("Error: Your inputs are not in ascending order.");
        error++;
        break;
      }
    } // end of ascending order error test 
    if (error==0){
      /*for (int i=0; i<15; i++){ // prints out original grade input 
        System.out.print(original[i] + " ");
      }*/
      System.out.println("Enter a grade to search for:");
      int search = scan.nextInt();
      binarySearch(original, search);
      scramble(original);
      System.out.println("Enter a grade to search for: ");
      int searchTwo= scan.nextInt();
      linearSearch(scramble(original), searchTwo);
    }
  }
}