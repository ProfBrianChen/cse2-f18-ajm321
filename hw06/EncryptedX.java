// Anna Moragne ajm321
// HW06 10-24-2018
// Purpose: Practice using nested loops to create an encrypted X within stars
import java.util.Scanner;
public class EncryptedX{
  public static void main (String [] args){
    Scanner scan=new Scanner (System.in);
    System.out.println("Enter an integer from 0-100");
    boolean integer=scan.hasNextInt(); // testing whether input is integer or not 
    int size=0; // =scan.nextInt(); // decalring the size of the grid for X
    int line=1; //the line of the grid
    int entry=1; // the row number of an entry within a line of the grid 
    while (line==1){
    while (integer ==true){
    if (integer==true){
      size=scan.nextInt(); // storing value only if true
      while (size>100){
        System.out.println("Your input was outside of the range requested. Please try again and enter an integer from 0-100");
        size=scan.nextInt(); // storing new value 
      }
      for (line=1; line<=size; line++){
        for (entry=1; entry<=size; entry++){
          if (entry!=line && entry!=(size-line)){
            System.out.print("*");
          }
          else {
            System.out.print(" "); // empty for X
          }
          if (entry==size){
            System.out.println(""); // empty for X
          }
        }
      }
      break;
    }
     //else {
      //scan.next();
     //}
    } // closes inner while (integer==true)
    while (integer==false){
     // scan.next();
      System.out.println("Your input was invalid. Please try again and enter an integer from 0-100");
      integer=scan.hasNextInt();
      if (integer==false) {
      scan.next();
      } // closes if 
    } //closes while (integer==false)
    } // closes biggest while loop
  } // closes main
} // closes class

          
        