// Anna Moragne ajm321
// CSE02 HW05 10-09-2018
// Purpose: Use while loops to generate hands in poker and test if they are a special type of poker hand
// Addittionaly print probability of special types of poker hands
import java.util.Scanner;
public class Hw05 {
  public static void main (String [] args) {
    Scanner scan = new Scanner (System.in);
    System.out.println("How many poker hands would you like to test? Please enter in the form of an integer");
    // asks user for input fot handswanted
    boolean handsTrue = scan.hasNextInt(); // test if input is an integer
    double handsWanted = scan.nextInt(); //stores number of hands wanted to run
    double currentHand = 1; // initalizing current hand being produced. Hand # will increase through loop
    int cardOne = 0;
    int cardTwo = 0;
    int cardThree = 0;
    int cardFour = 0;
    int cardFive = 0;
    // initalizing the 5 cards in a hand
    int cardValOne = 0;
    int cardValTwo = 0;
    int cardValThree = 0;
    int cardValFour = 0;
    int cardValFive = 0;
    // initalizing the face value
    String suitOne = ("");
    String suitTwo = ("");
    String suitThree = ("");
    String suitFour = ("");
    String suitFive = ("");
    // initalizing suit for each of the 5 cards
    String faceOne = ("");
    String faceTwo = ("");
    String faceThree = ("");
    String faceFour = ("");
    String faceFive = ("");
    // initalizing face value for each of the 5 cards
    double fourOfAKind = 0;
    double threeOfAKind = 0;
    double twoPair = 0;
    double onePair = 0;
    // the possible special cases
    double propFourKind = 0;
    double propThreeKind = 0;
    double propTwoPair = 0;
    double propOnePair = 0;
    // will use to find proportion of special hands
    if (handsTrue == false){
      while (handsTrue == false){
      scan.next();
      System.out.println("The number of hands that you inputed is invalid. Please try again with an integer value");
      handsWanted = scan.nextInt();
      }
    }
    else if (handsTrue == true){
     // handsWanted = scan.nextInt();
      while (currentHand <= handsWanted){
        cardOne = (int)(Math.random() * 52 + 1 );
        cardTwo = (int)(Math.random() * 52 + 1 );
        cardThree = (int)(Math.random() * 52 + 1 );
        cardFour = (int)(Math.random() * 52 +1);
        cardFive = (int)(Math.random() * 52 +1);
      // randomly assigning which card has been drawn from the deck 
        while (cardOne == cardTwo || cardOne == cardThree || cardOne == cardFour || cardOne == cardFive || cardTwo == cardThree || cardTwo == cardFour || cardTwo == cardFive || cardThree == cardFour || cardThree == cardFive || cardFour == cardFive){
          // if any of the cards were the same we will randomly generate a new hand of cards
          cardOne = (int)(Math.random() * 52 + 1 );
          cardTwo = (int)(Math.random() * 52 + 1 );
          cardThree = (int)(Math.random() * 52 + 1 );
          cardFour = (int)(Math.random() * 52 +1);
          cardFive = (int)(Math.random() * 52 +1);
          // re-drawing a hand of cards
        }
        if (cardOne < 14){
          suitOne = ("diaonds");
        }
        else if (14 >= cardOne && cardOne <=26){
          suitOne = ("clubs");
        }
        else if (27 >= cardOne && cardOne <= 39){
          suitOne = ("hearts");
        }
        else if (cardOne <= 40){
          suitOne = ("spades");
        }
        // setting the suit for card one
        if (cardTwo < 14){
          suitTwo = ("diaonds");
        }
        else if (14 >= cardTwo && cardTwo <=26){
          suitTwo = ("clubs");
        }
        else if (27 >= cardTwo && cardTwo <= 39){
          suitTwo = ("hearts");
        }
        else if (cardTwo >= 40){
          suitTwo = ("spades");
        }
        // setting the suit for card two
        if (cardThree < 14){
          suitThree = ("diaonds");
        }
        else if (14 >= cardThree && cardThree <=26){
          suitThree = ("clubs");
        }
        else if (27 >= cardThree && cardThree<= 39){
          suitThree = ("hearts");
        }
        else if (cardThree >= 40){
          suitThree = ("spades");
        }
        // setting the suit for card three
        if (cardFour < 14){
          suitFour = ("diaonds");
        }
        else if (14 >= cardFour && cardFour <=26){
          suitFour = ("clubs");
        }
        else if (27 >= cardFour && cardFour <= 39){
          suitFour = ("hearts");
        }
        else if (cardFour >= 40){
          suitFour = ("spades");
        }
        // setting the suit for card four
        if (cardFive <= 13){
          suitFive = ("diaonds");
        }
        else if (14 >= cardFive && cardFive <=26){
          suitFive = ("clubs");
        }
        else if (27 >= cardFive && cardFive <= 39){
          suitFive = ("hearts");
        }
        else if (cardFive >= 40){
          suitFive = ("spades");
        }
        // setting the suit for card five
        cardValOne = cardOne % 13;
        cardValTwo = cardTwo % 13;
        cardValThree = cardThree % 13;
        cardValFour = cardFour % 13;
        cardValFive = cardFive % 13;
        // will help find face value of each card
        if (cardValOne == 1){
          faceOne = ("Ace");
        }
        else if (cardValOne == 0){
          faceOne = ("King");
        }
        else if (cardValOne == 12){
          faceOne = ("Queen");
        }
        else if (cardValOne == 11){
          faceOne = ("Jack");
        }
        else {
          faceOne = Integer.toString(cardValOne);
        }
        // decalring the face value of card one
        if (cardValTwo == 1){
          faceTwo = ("Ace");
        }
        else if (cardValTwo == 0){
          faceTwo = ("King");
        }
        else if (cardValTwo == 12){
          faceTwo = ("Queen");
        }
        else if (cardValTwo == 11){
          faceTwo = ("Jack");
        }
        else {
          faceTwo = Integer.toString(cardValTwo);
        }
        // decalring the face value of card two
        if (cardValThree == 1){
          faceThree = ("Ace");
        }
        else if (cardValThree == 0){
          faceThree = ("King");
        }
        else if (cardValThree == 12){
          faceThree = ("Queen");
        }
        else if (cardValThree == 11){
          faceThree = ("Jack");
        }
        else {
          faceThree = Integer.toString(cardValThree);
        }
        // decalring the face value of card three
        if (cardValFour == 1){
          faceFour = ("Ace");
        }
        else if (cardValFour == 0){
          faceFour = ("King");
        }
        else if (cardValFour == 12){
          faceFour = ("Queen");
        }
        else if (cardValFour == 11){
          faceFour = ("Jack");
        }
        else {
          faceFour = Integer.toString(cardValFour);
        }
        // decalring the face value of card four
        if (cardValFive == 1){
          faceFive = ("Ace");
        }
        else if (cardValFive == 0){
          faceFive = ("King");
        }
        else if (cardValFive == 12){
          faceFive = ("Queen");
        }
        else if (cardValFive == 11){
          faceFive = ("Jack");
        }
        else {
          faceFive = Integer.toString(cardValFive);
        }
        // decalring the face value of card five
        if ((suitOne == suitTwo && suitTwo == suitThree && suitThree == suitFour) || (suitOne == suitTwo && suitTwo == suitThree && suitThree == suitFive) || (suitOne == suitThree && suitThree == suitFour && suitFour == suitFive) || (suitOne == suitTwo && suitTwo == suitFour && suitFour == suitFive) || (suitTwo == suitThree && suitThree == suitFour && suitFour == suitFive)){
          // if there is a four of a kind 
          fourOfAKind++ ;
        }
        else if ((suitOne == suitTwo && suitTwo == suitThree) || (suitOne == suitTwo && suitTwo == suitFour) || (suitOne == suitTwo && suitTwo == suitFive) || (suitOne == suitThree && suitThree == suitFour) || (suitOne == suitThree && suitThree == suitFive) || (suitOne == suitFour && suitFour == suitFive) || (suitTwo == suitThree && suitThree == suitFive) || (suitTwo == suitThree && suitThree == suitFour) || (suitTwo == suitFour && suitFour == suitFive) || (suitThree == suitFour && suitFour == suitFive)){
          // if there us a three of a kind
          threeOfAKind++ ;
        }
        if (faceOne == faceTwo){
          if (faceThree == faceFour || faceThree == faceFive || faceFour == faceFive){
            twoPair++ ;
          }
          else {
            onePair++ ;
          }
        }
        else if (faceOne == faceThree){
          if (faceTwo == faceFour || faceTwo == faceFive || faceFour == faceFive){
              twoPair++ ;
          }
          else {
            onePair++ ;    
          }
        }
        else if (faceOne == faceFour){
          if (faceTwo == faceFive || faceThree == faceFive || faceTwo == faceThree){
            twoPair++ ;
          }
          else {
            onePair++ ;
          }
        }
        else if (faceTwo == faceThree){
          if (faceFour == faceFive || faceFive == faceOne){
            twoPair++ ;
          }
          else {
            onePair++ ;
          }
        }
        else if (faceTwo == faceFour){
          if (faceOne == faceFive || faceFive == faceThree){
            twoPair++ ;
          }
          else {
            onePair++ ;
          } 
        }
        else if (faceThree == faceFour){
          if (faceOne == faceFive || faceTwo == faceFive){
            twoPair++ ;
          }
          else {
            onePair++ ;
          }
        }
        // determines if there are any two pairs or one pairs 
        currentHand++ ; 
        // will return to begining of while loop with new hand
      }
      propFourKind = (fourOfAKind/handsWanted);
      propThreeKind = (threeOfAKind/handsWanted);
      propTwoPair = (twoPair/handsWanted);
      propOnePair = (onePair/handsWanted);
      System.out.println("Four of a kind : " + fourOfAKind + " Three of a kind:" + threeOfAKind + " Two pair:" + twoPair + " One pair:" + onePair);
      System.out.println("The number of poker hands generated was " + handsWanted);
      System.out.printf("The proportion of Four of a Kinds: %.3f" , propFourKind);
      System.out.printf("The proportion of Three of a Kinds: %.3f" , propThreeKind);
      System.out.printf("The proportion of Two Pairs: %.3f" , propTwoPair);
      System.out.printf("The proportion of One Pairs: %.3f" , propOnePair);
    }
  }
}

      
      