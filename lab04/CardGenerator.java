// Anna Moragne ajm321
// lab 04  09-20-18
// Purpose: Practice using if and switch statements as well as a random number generator 
// Randomly select a card from a standard deck of card and read out which card it is 
public class CardGenerator {
  public static void main (String [] args) {
    int card = (int)(Math.random() * 52 + 1);
    // randomly generates a card 
    String suit = ""; // variable for the suit of the card 
    String number = ""; // variable for the number or face of the card 
        if (card < 14){
          // setting diamonds suit 
          suit = "Diamonds";
        }
        else if (card >= 14 & card < 27){
          // setting clubs suit
          suit = "Clubs"; 
        }
        else if (card >= 27 & card < 40){
          // setting hearts suit 
          suit = "Hearts"; 
        }
        else if (card > 39){
          // setting spades suit 
          suit = "Spades";
        }
    int cardVal = (card + 13) % 13;
   if (cardVal == 11){
     number = "Jack"; // setting jacks
   }
    else if (cardVal == 12){
      number = "Queen"; // setting queens 
    }
    else if (cardVal == 13){
      number = "King"; // setting kings 
    }
    else if (cardVal == 1){
      number = "Ace"; // setting aces 
    }
    else if (cardVal == 2){
      number = "2";
    }
    else if (cardVal == 3){
      number = "3";
    }
    else if (cardVal == 4){
      number = "4";
    }
    else if (cardVal == 5){
      number = "5";
    }
    else if (cardVal == 6){
      number = "6";
    }
    else if (cardVal == 7){
      number = "7";
    }
    else if (cardVal == 8){
      number = "8";
    }
    else if (cardVal == 9){
      number = "9";
    }
    else if (cardVal == 10){
      number = "10";
    }
    System.out.println ("You picked the " + number + " of " + suit);
  }
}

    
        
        