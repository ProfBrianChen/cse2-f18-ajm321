///////////////////////////
/// Anna Moragne CSE 02
/// Homework 02 due 09-11-2018
/// Purpose: Manipulating data stored in variables, running simple calculations, and printing numerical output that was generated
public class Arithmetic {
  public static void main (String [] args) {
    // Number of pairs of pants 
    int numPants = 3;
    // Cost per pair of pants
    double pantsPrice = 34.98;
    // Number of sweatshirts
    int numShirts =2;
    // Cost per shirt 
    double shirtPrice = 24.99;
    // Number of belts 
    int numBelts =1;
    // Cost per belt
    double beltCost = 33.99;
    // The tax rate
    double paSalesTax = 0.06;
    /// Declaring variables for wanted results
    double totalCostOfPants, totalCostOfShirts, totalCostOfBelts; // Total cost of each type of item
    double salesTaxPants, salesTaxShirts, salesTaxBelts; // The PA Sales tax on each type of item
    double totalCostBeforeTax; // combined purchase total before added tax
    double totalSalesTax; // total sales tax on purchase
    double totalCost; // total cost including tax
    totalCostOfPants=numPants*pantsPrice; 
    //calculates total price of pants before tax
    totalCostOfShirts=numShirts*shirtPrice;
    // calculates total price of sweatshirts before tax
    totalCostOfBelts=numBelts*beltCost;
    // calculates total cost of belst before tax 
    salesTaxPants=totalCostOfPants*paSalesTax;
    salesTaxShirts=totalCostOfShirts*paSalesTax;
    salesTaxBelts=totalCostOfBelts*paSalesTax;
    totalCostBeforeTax=totalCostOfPants+totalCostOfShirts+totalCostOfBelts;
    // calculates total cost before tax is added
    double newSalesTaxPants= Math.round(salesTaxPants*100.00)/100.00; // rounding decimals on pants to two decimals
    double newSalesTaxShirts= Math.round(salesTaxShirts*100.00)/100.00; // rounding tax on shirts to two decimals
    double newSalesTaxBelts= Math.round(salesTaxBelts*100.00)/100.00; // rounding tax on belts to two decimals 
        totalSalesTax=newSalesTaxPants+newSalesTaxShirts+newSalesTaxBelts;
    // calculates total sales tax on purchase
        totalCost=totalCostBeforeTax+totalSalesTax;
    // calculates total cost of purchase
    System.out.println ("The pants cost $"+(totalCostOfPants)+" and were taxed $"+(newSalesTaxPants));
    System.out.println ("The sweatshirts cost $"+(totalCostOfShirts)+" and were taxed $"+(newSalesTaxShirts));
    System.out.println ("The belt cost $"+(totalCostOfBelts)+" and was taxed $"+(newSalesTaxBelts));
    // Displays the total cost of each type of item as well as the associated sales tax
    System.out.println ("Before tax was added, the total cost of the purchase was $"+(totalCostBeforeTax));
    System.out.println ("The total amount of sales tax on the purchase was $"+(totalSalesTax));
    System.out.println ("The total cost of the purchase, including sales tax, was $"+(totalCost));
  }
}

    
    
    
    
    
    
    
    
    
    
    
    