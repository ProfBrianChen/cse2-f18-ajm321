// Anna Moragne     ajm321
// CSE 002  lab 09    
// 11/15/2018
public class lab09 {
  public static int [] copy (int [] arrayIn){
    int i=0;
    int [] arrayOut = new int [arrayIn.length];
    for (i=0; i<arrayIn.length; i++){
      arrayOut[i] = arrayIn[i];
    }
    return arrayOut;
  }
  public static void inverter (int [] arrayIn){
    int k =0;
    for (k=0; k<arrayIn.length/2; k++){
      int temp = arrayIn[k];
      arrayIn[k]= arrayIn[arrayIn.length-1-k];
      arrayIn[arrayIn.length-1-k] = temp;
    }
  }
  public static int [] inverter2 (int [] list){
    int [] output;
    inverter(copy (list));
    output = list;
    return output;
  }
  public static void print (int [] array){
    int i=0;
    int length=array.length;
    for (i=0; i<length; i++){
      System.out.print(array[i] + ", ");
    }
    System.out.println(" ");
  }
  public static void main (String [] args){
    int [] array0 = new int [10];
    int i=0;
    for (i=0; i<10; i++){
      array0[i]=i*2;
    }
    int [] array1 = copy(array0);
    int [] array2 = copy(array0);
    inverter(array0);
    print(array0);
    print(inverter2(array1));
    int [] array3 = inverter2(array0);
    print(array3);
  }
}