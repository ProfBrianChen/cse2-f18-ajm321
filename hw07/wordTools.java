// Anna Moragne   ajm321
// CSE 002    hw07    10-30-2018
// Purpose: Practice writing and using methods
//          return information about the paragraph that the user inputs
import java.util.Scanner;
public class wordTools{
  public static String sampleText(){
    Scanner scan = new Scanner (System.in);
    System.out.println("Enter a sample text");
    String paragraph=scan.nextLine();
    return paragraph;
  }
  public static char printMenu(){
    Scanner scan = new Scanner (System.in);
    char choice;
    System.out.println("Menu:");
    System.out.println("c - Number of non-whotespace charecters");
    System.out.println("w - Number of words");
    System.out.println("f - Find text");
    System.out.println("r - Replace all ! 's");
    System.out.println("s - Shorten spaces");
    System.out.println("q - Quit");
    System.out.println("Enter the letter corresponding to your choive");
    choice=scan.next().charAt(0);
    System.out.println("You chose " + choice);
    return choice;
  }
  public static int getNumOfNonWSCharacters(String paragraph){
    int totalCharacters = paragraph.length();// the number of all characters in the pragraph 
    int i=0; // the chracatrer position in the paragrph;
    int totalWhiteSpace=0; // will add up to total number of white spaces
    for (i=0; i<totalCharacters; i++){
     // System.out.print("i; " + i);
     // System.out.println("total characters: " + totalCharacters);
      boolean whiteSpaceTruth = Character.isWhitespace(paragraph.charAt(i));
      if (whiteSpaceTruth==true){
        totalWhiteSpace++;
      }
    }
    int nonWSCharacters = (totalCharacters - totalWhiteSpace);
    return nonWSCharacters;
  }
  public static int getNumOfWords(String paragraph){
    int numOfWords;
    int totalWhiteSpace=0;
    int totalCharacters = paragraph.length();
    int i=0;
    char let;
    for (i=0; i<totalCharacters; i++){
      let = paragraph.charAt(i);
      boolean whiteSpaceTruth = Character.isWhitespace(let);
      if (whiteSpaceTruth==true){
        totalWhiteSpace++;
      }
    }
    numOfWords=(totalWhiteSpace + 1);
    return numOfWords;
  }
  
  // everything above compiles 
  
  public static int findText(String paragraph, String word){
    int totalCharacters = paragraph.length();
    int index = 0;
    int i =0; // will count the number of times a word occurs
    for (index=0; index<=totalCharacters; index=(index+1)){
      index = paragraph.indexOf(word, index);
      if (index==-1){
        i=0;
        break;
      }
      else {
        i++;
      }
    }
    return i;
  }
  public static String replaceExclamation(String paragraph, String replacement){
    String newParagraph = paragraph.replace("!", replacement);
    return newParagraph;
  }
  public static String shortenSpace(String paragraph){
    String newParagraph = paragraph.replace("  ", " ");
    String newParagraphTwo = newParagraph.replace("   ", " ");
    return newParagraphTwo;
  }
  public static void main (String [] args){
    Scanner scan = new Scanner (System.in);
    char choiceOfficial;
    String textOfficial = sampleText();
    System.out.println("You entered: " + textOfficial);
    choiceOfficial = printMenu();
    // System.out.print(printMenu);
    if (choiceOfficial=='c'){
      System.out.println("Number of non-whitespace characters: " + getNumOfNonWSCharacters(textOfficial));
    }
    else if (choiceOfficial=='w'){
      System.out.println("Number of words:" + getNumOfWords(textOfficial));
    }
    else if (choiceOfficial=='f'){
      System.out.println("Enter a word or phrase to be found:");
      String word=scan.nextLine();
      System.out.println(word + " instances: " + findText(textOfficial, word));
    }
    else if (choiceOfficial=='r'){
      System.out.println("enter a letter or character you would like to replace ! with");
      String replacement=scan.nextLine();
      System.out.println(replaceExclamation(textOfficial, replacement));
    }
    else if (choiceOfficial=='s'){
      System.out.print(shortenSpace(textOfficial));
    }
    else if (choiceOfficial=='q'){
      System.out.println("No information to output");
    }
    else {
      System.out.println("No information to output");
    }
  } 
   
  }
  
  
  
  
  
  
  
  
  
    