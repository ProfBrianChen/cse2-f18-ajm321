// Anna Moragne ajm321
// lab 05   10-04-2018
// Purpose: practice using loop statements to determine if user inputs the correct type of data
import java.util.Scanner;
public class Lab05 {
  public static void main (String [] args) {
    Scanner scan=new Scanner (System.in);
    System.out.println("Choose a class you are currently enrolled in. What is the course number?");
    // asking for input on course number
    boolean numberTruth = scan.hasNextInt();
    boolean departmentTruth=scan.hasNextLine();
    boolean meetTruth = scan.hasNextInt();
    // determining whether or not the value input by ther user is an integer
    int cNumber =0;
    String department = (""); // test new method 
    int meetAmount =0; // test new method
    String time = ("");
    String prof = ("");
    int studentNumber = 0;
    if (numberTruth == true){
      cNumber = scan.nextInt();
    }
    else
    {
      scan.next(); // clears value for scanner if not true
    }
    // storing value of course number
    while (numberTruth == false){
      System.out.println("What you entered in not an integer. Please input the course number again in the form of an integer");
      numberTruth = scan.hasNextInt();
      if (numberTruth == true){
        cNumber = scan.nextInt();
      }
      else {
      scan.next();
      }
    }
    if (numberTruth == true){ 
      System.out.println("Which department is this course in?"); // ask for department input
      departmentTruth = scan.hasNextLine(); // used to be "boolean departmentTruth = scan.hasNextLine();"
    // String department = ("");
      if (departmentTruth == true){
        department = scan.next();
        System.out.println("How many times does the class meet per week?");
        meetTruth = scan.hasNextInt();
        if (meetTruth == true){
          meetAmount = scan.nextInt();
          System.out.println("What time does the class start? Enter in form xx:xx am/pm");
          boolean timeTruth = scan.hasNextLine();
          if (timeTruth == true){
            time = scan.next();
            System.out.println("What is the name of the instructor who teaches this course?");
            boolean profTruth = scan.hasNextLine();
            if (profTruth == true){
              prof = scan.next();
              System.out.println("How many students are in the course?");
              boolean studentTruth = scan.hasNextInt();
              if (studentTruth == true){
                studentNumber = scan.nextInt();
              }
              else if (studentTruth == false){
                scan.next();
                while (studentTruth == false){
                  System.out.println("The number of students you entered is invalid. Please enter again in the form of an integer");
                   studentTruth = scan.hasNextInt();
                  if (studentTruth == true){
                    studentNumber = scan.nextInt();
                  }
                }
              }
            else if (profTruth == false){
              scan.next();
              while (profTruth == false){
                System.out.println("The name of the professor you entered is invalid. Please try again");
                profTruth = scan.hasNextLine();
                if (profTruth == true){
                  prof = scan.nextLine();
                }
              }
            }
            }
          else if (timeTruth == false){
            scan.next();
            while (timeTruth == false){
              System.out.println("The time you entered is invalid. Please enter the time you start in the form xx:xx am/pm");
              timeTruth = scan.hasNextLine();
              if (timeTruth == true){
                time = scan.nextLine();
              }
            }
          }
          }
        else if (meetTruth == false){
          scan.next();
          while (meetTruth == false){
            System.out.println("The number of times you entered to meet is invalid. Please input the amount of times you meet in the form of an integer");
            meetTruth = scan.hasNextInt ();
            if (meetTruth == true){
              meetAmount = scan.nextInt();
            }
          }
        }
        }
      else if (departmentTruth == false){
        scan.next();
        while (departmentTruth == false){
          System.out.println("How many times does the class meet per week?");
          departmentTruth = scan.hasNextLine();
          if (departmentTruth == true){
            department = scan.nextLine();
          }
        }
      }
      }
      System.out.println("Course Number: " + cNumber);
      System.out.println("The Department this course is in is " + department);
      System.out.println("The Professor who teaches this course is " + prof);
      System.out.println("The class meets " + meetAmount + " times per week and starts at " + time);
      System.out.println("There are " + studentNumber + " students in the class");
      }
    }
  }
