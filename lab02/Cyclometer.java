//////////////
// Anna Moragne CSE 02
// Lab 02 
// 09-06-2018
/// Purpose: record time elapsed, number of rotations of bicycle wheel
/// Purpose: print number of minutes, number of counts, distance in miles, distance of two trips combines
// Document your program. What does MPG do? Place your comments here!
//
public class Cyclometer { 
  // main method required for every Java program
  public static void main (String [] args) {
    int secsTrip1=480; // integer variable counts seconds of Trip 1
    int secsTrip2=3220; // integer variable counts seconds of Trip 2
    int countsTrip1=1561; // integer variable counts distance of Trip 1
    int countsTrip2=9037; // integer variable counts distace of Trip 2
    double wheelDiameter=27.0, // wheel diameter
    PI=3.14159, // pi value constant
    feetPerMile=5280, // feet in a mile constant
    inchesPerFoot=12, // inches per foot constant
    secondsPerMinute=60; // seconds per minute constant
    double distanceTrip1, distanceTrip2,totalDistance; //all distance values are in decimal form
    System.out.println ("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    System.out.println ("Trip 2 took "+ (secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+"counts.");
    //run the calculations; store the values. Document calculations
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    // Above gives distance in inches 
    // for each count, a rotation of the wheel travels the diameter in inches times PI 
    distanceTrip1/=inchesPerFoot*feetPerMile; // gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance=distanceTrip1+distanceTrip2;
    // Print out the output data
    System.out.println ("Trip 1 was "+distanceTrip1+" miles");
    System.out.println ("Trip 2 was "+distanceTrip2+" miles");
    System.out.println ("The total distance was "+totalDistance+" miles");
  } //end of main method
} //end of class