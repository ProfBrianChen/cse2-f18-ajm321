// Lab 03 CSE 02
// Anna Moragne ajm321
// 09-13-2018
/// Practice using the scanner class 
/// Find the amount of the bill, the number of ways the check will be split, and the amount each person in a group will need to pay
import java.util.Scanner;
// Documents your program
// Imports information from scanner 
public class Check {
  // main method required for every Java program
  public static void main (String[] args) {
Scanner myScanner = new Scanner ( System.in  ) ;
// accepting input by declaring the scanner
System.out.print("Enter the original cost of the check in the form xx.xx ");
// asks users to manualy input the cost of the check
double checkCost = myScanner.nextDouble();
// accepting user input in the form of a double
System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ") ;
// prompts user to manualy input percent they wish to tip 
double tipPercent = myScanner.nextDouble();
// declaring tip percentage variable 
tipPercent /=100; // We want to convert the percentage into a decimal value
  System.out.print("Enter the number of people who went out to dinner: ");
// prompts user to manualy input the number of people who ate dinner together, i.e. the number of ways the check will be split
int numPeople = myScanner.nextInt();
// declaring the number of people at dinner as an integer value
double totalCost; // declaring the total cost variable as a double
double costPerPerson; // declaring the amount each individual must pay as a double variable
int dollars, //whole dollar amount of costPerPerson
dimes, pennies; //for storing digits to the right of the decimal point for the cost $
totalCost = checkCost * (1+tipPercent);
costPerPerson = totalCost / numPeople;
//get the whole amount, dropping decimal fraction
dollars=(int)costPerPerson;
//get dimes amount, e.g.,
// the % (mod) operator returns the remainder after the division
dimes=(int)(costPerPerson * 10) % 10;
pennies=(int)(costPerPerson * 100) % 10;
System.out.println ("Each person in the group owes $" + dollars + '.' + dimes + pennies) ; 
  } // end of main method 
} // end of class