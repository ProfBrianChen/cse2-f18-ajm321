// Anna Moragne ajm321
// Lab 07 10-25-2018
import java.util.Random; // importing random method
import java.util.Scanner;
public class Methods{
    public static String adjOne (int random) {
      //random = randomGenerator.nextInt(10);
      String adjectiveOne="";
      switch (random){
        case 0:
          adjectiveOne="sad";
          break;
        case 1:
          adjectiveOne="sad";
          break;
        case 2:
          adjectiveOne="excited";
          break;
        case 3:
          adjectiveOne="dangerous";
          break;
        case 4:
          adjectiveOne="tired";
          break;
        case 5:
          adjectiveOne="busy";
          break;
        case 6:
          adjectiveOne="slow";
          break;
        case 7:
          adjectiveOne="loud";
          break;
        case 8:
          adjectiveOne="lazy";
          break;
        case 9:
          adjectiveOne="studious";
          break;
      }
      return adjectiveOne;
    }
      // finds random first adjective
    public static String adjTwo (int random) {
      //random = randomGenerator.nextInt(10);
      String adjectiveTwo="";
      switch (random){
        case 0:
          adjectiveTwo="purple";
          break;
        case 1:
          adjectiveTwo="purple";
          break;
        case 2:
          adjectiveTwo="ugly";
          break;
        case 3:
          adjectiveTwo="gorgeous";
          break;
        case 4:
          adjectiveTwo="yellow";
          break;
        case 5:
          adjectiveTwo="brown";
          break;
        case 6:
          adjectiveTwo="upbeat";
          break;
        case 7:
          adjectiveTwo="clumsy";
          break;
        case 8:
          adjectiveTwo="green";
          break;
        case 9:
          adjectiveTwo="crazy";
          break;
      }
      return adjectiveTwo;
    }
      // finds a random second adjective
    public static String adjThree (int random) {
      //random = randomGenerator.nextInt(10);
      String adjectiveThree="";
      switch (random){
        case 0:
          adjectiveThree="upset";
          break;
        case 1:
          adjectiveThree="upset";
          break;
        case 2:
          adjectiveThree="spontaenous";
          break;
        case 3:
          adjectiveThree="wonderful";
          break;
        case 4:
          adjectiveThree="distressed";
          break;
        case 5:
          adjectiveThree="fast";
          break;
        case 6:
          adjectiveThree="energetic";
          break;
        case 7:
          adjectiveThree="wild";
          break;
        case 8:
          adjectiveThree="funny";
          break;
        case 9:
          adjectiveThree="calm";
          break;
      }
      return adjectiveThree;
    }
      // finding a random third adjective
    public static String nounA (int random) {
     // random = randomGenerator.nextInt(10);
      String nounOne="";
      switch (random){
        case 0:
          nounOne="table";
          break;
        case 1:
          nounOne="table";
          break;
        case 2:
          nounOne="elephant";
          break;
        case 3:
          nounOne="kangaroo";
          break;
        case 4:
          nounOne="couch";
          break;
        case 5:
          nounOne="spider";
          break;
        case 6:
          nounOne="butterfly";
          break;
        case 7:
          nounOne="child";
          break;
        case 8:
          nounOne="girl";
          break;
        case 9:
          nounOne="boy";
          break;
      }
      return nounOne;
    }
      // finding a random first noun
    public static String nounB (int random) { 
      //random = randomGenerator.nextInt(10);
      String nounTwo="";
      switch (random){
        case 0:
          nounTwo="teenager";
          break;
        case 1:
          nounTwo="teenager";
          break;
        case 2:
          nounTwo="bug";
          break;
        case 3:
          nounTwo="flower";
          break;
        case 4:
          nounTwo="cow";
          break;
        case 5:
          nounTwo="caterpiller";
          break;
        case 6:
          nounTwo="cartoon";
          break;
        case 7:
          nounTwo="plant";
          break;
        case 8:
          nounTwo="mother";
          break;
        case 9:
          nounTwo="giraffe";
          break;
      }
      return nounTwo;
    }
      // finding a random second nounTwo
    public static String verbA (int random) {
    //  random = randomGenerator.nextInt(10);
      String verb="";
      switch (random){
        case 0:
          verb="jumpped";
          break;
        case 1:
          verb="jumpped";
          break;
        case 2:
          verb="skipped";
          break;
        case 3:
          verb="ran past";
          break;
        case 4:
          verb="sang to";
          break;
        case 5:
          verb="passed";
          break;
        case 6:
          verb="climbed";
          break;
        case 7:
          verb="loved";
          break;
        case 8:
          verb="flatened";
          break;
        case 9:
          verb="created";
          break;
      }
      return verb;
    }
     public static String verbB (int random) {
    //  random = randomGenerator.nextInt(10);
      String verbTwo="";
      switch (random){
        case 0:
          verbTwo="rude";
          break;
        case 1:
          verbTwo="mean";
          break;
        case 2:
          verbTwo="rude";
          break;
        case 3:
          verbTwo="excited";
          break;
        case 4:
          verbTwo="running";
          break;
        case 5:
          verbTwo="singing";
          break;
        case 6:
          verbTwo="thinking";
          break;
        case 7:
          verbTwo="performing";
          break;
        case 8:
          verbTwo="whispering";
          break;
        case 9:
          verbTwo="dancing";
          break;
      }
      return verbTwo;
    }
    public static String adverbA (int random) {
    //  random = randomGenerator.nextInt(10);
      String adverb="";
      switch (random){
        case 0:
          adverb ="particularly";
          break;
        case 1:
          adverb="particularly";
          break;
        case 2:
          adverb="suspiciously";
          break;
        case 3:
          adverb="accidently";
          break;
        case 4:
          adverb="boringly";
          break;
        case 5:
          adverb="confidently";
          break;
        case 6:
          adverb="surprisingly";
          break;
        case 7:
          adverb="affirmitvley";
          break;
        case 8:
          adverb="directly";
          break;
        case 9:
          adverb="dully";
          break;
      }
      return adverb;
     }
        public static String action (String temporarySubject, String placeA, String placeB, String placeC, String placeD){
          return ("The " + temporarySubject + " was " + placeA + " " + placeB+ " to the " + placeC + " " + placeD + "."); 
        }
     public static String conclusion (String fillerA, String fillerB){
       return ("That " + fillerA + " knew the " + fillerB + ".");
     }
    public static void main (String [] args){
      Random randomGenerator = new Random (); // declaring new random object 
      Scanner scan = new Scanner (System.in);
      System.out.println("How many action statements would you like? Enter an integer less than 6.");
      int numAction=scan.nextInt();
      int current = 1;
      //int again = 1;
      int random = 0;
     // while (again ==1) {
      random = randomGenerator.nextInt(10);
      System.out.println("The " + adjOne(random)+", "+adjTwo(random) + " " + nounA(random) + " " + verbA(random) + " the " + adjThree(random) + " " + nounB(random)+".");
     // System.out.println("Would you like to generate another sentence? If yes enter 1, if no enter 2");
      //again = scan.nextInt();
      //}
      int randomTwo = randomGenerator.nextInt(10);
      int another;
      int anotherTwo;
      for (current=1; current<=numAction; current++){
        another = randomGenerator.nextInt(10);
        anotherTwo = randomGenerator.nextInt(10);
        System.out.println(action(nounA(another), adverbA(anotherTwo), verbA(anotherTwo), verbB(another), nounB(anotherTwo)));
      }
      System.out.println(conclusion(nounA(random), nounB(random)));
    }
}
      
    
      
    
    
    
    